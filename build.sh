# RootDir     = /opt/ArchARM
# DBPath      = /opt/ArchARM/var/lib/pacman/
# CacheDir    = /opt/ArchARM/var/cache/pacman/pkg/
# GPGDir      = /opt/ArchARM/etc/pacman.d/gnupg/
# HookDir     = /opt/ArchARM/etc/pacman.d/hooks/
# Architecture = aarch64


# --root /opt/ArchARM
# --dbpath /opt/ArchARM/var/lib/pacman/
# --cachedir /opt/ArchARM/var/cache/pacman/pkg/
# --gpgdir /opt/ArchARM/etc/pacman.d/gnupg/
# --hookdir /opt/ArchARM/etc/pacman.d/hooks/
# --arch aarch64

# pacman -S --root /opt/ArchARM --dbpath /opt/ArchARM/var/lib/pacman/ --cachedir /opt/ArchARM/var/cache/pacman/pkg/ --gpgdir /opt/ArchARM/etc/pacman.d/gnupg/ --hookdir /opt/ArchARM/etc/pacman.d/hooks/ --arch aarch64 gtk3

# set -x PKG_CONFIG_SYSROOT_DIR /opt/ArchARM/
# set -x PKG_CONFIG_ALLOW_CROSS 1
# set -x PKG_CONFIG_PATH /opt/ArchARM/lib/pkgconfig/
# set -x CARGO_BUILD_TARGET aarch64-unknown-linux-gnu
# set -x CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER aarch64-linux-gnu-gcc
# #PKG_CONFIG_ALLOW_CROSS=1
#  cargo build --target aarch64

PKG_CONFIG_SYSROOT_DIR=/opt/ArchARM/
PKG_CONFIG_ALLOW_CROSS=1
PKG_CONFIG_PATH=/opt/ArchARM/lib/pkgconfig/
CARGO_BUILD_TARGET=aarch64-unknown-linux-gnu
CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER=aarch64-linux-gnu-gcc
PKG_CONFIG_ALLOW_CROSS=1 cargo build --target aarch64-unknown-linux-gnu
